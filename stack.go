package sip

import (
	"fmt"
	"math/rand"
	"net"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/content"
	"gitlab.com/jhutchins/go-sip/dialog"
	"gitlab.com/jhutchins/go-sip/header"
	"gitlab.com/jhutchins/go-sip/message"
	"gitlab.com/jhutchins/go-sip/uri"
)

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

type udpWriter struct {
	conn *net.UDPConn
	addr *net.UDPAddr
}

func (w *udpWriter) Respond(res *message.Response) {
	// TODO depending on dialog information, like Via and Route headers, the correct location to
	// respond to is not always the location the message was received from
	log.Debug("Sending response")
	result := fmt.Sprintf("SIP/2.0 %s %s\r\n", res.Status.Code, res.Status.Reason)
	for _, header := range res.Headers {
		result += fmt.Sprintf("%s: %s\r\n", header.Name(), header.Value())
	}
	result += "\r\n" + res.Body
	w.conn.WriteToUDP([]byte(result), w.addr)
}

func (w *udpWriter) Request(req *message.Request) {
	log.Debug("Sending request")
	result := fmt.Sprintf("%s %s SIP/2.0\r\n", req.Method, req.RURI)
	for _, header := range req.Headers {
		result += fmt.Sprintf("%s: %s\r\n", header.Name(), header.Value())
	}
	result += "\r\n" + req.Body
	w.conn.WriteToUDP([]byte(result), w.addr)
}

func (w *udpWriter) Host() string {
	return w.conn.LocalAddr().String()
}

func (s *Stack) bindUdp() error {
	conn, err := net.ListenUDP("udp", s.udp)
	if err != nil {
		return err
	}
	s.conn = conn
	log.WithField("address", s.Local()).Infof("Stack listening for UDP")
	go processUdp(conn, s)
	return nil
}

func processUdp(conn *net.UDPConn, s *Stack) {
	defer log.Info("UDP socket closed")
	defer conn.Close()
	for s.running {
		var buf [2048]byte

		n, addr, err := conn.ReadFromUDP(buf[0:])
		if err != nil {
			log.WithError(err).Error("Trouble reading from UDP")
			continue
		}
		input := string(buf[:n])
		log.WithFields(log.Fields{"source": addr, "message": input}).Debug("Received UDP message")

		req, e := message.ParseRequest(input)
		if e == nil {
			log.WithField("request", req).Debug("Sending request to handler")
			go s.manager.processRequest(&req, &udpWriter{conn: conn, addr: addr})
		} else {
			res, e := message.ParseResponse(input)
			if e == nil {
				log.WithField("response", res).Debug("Sending response to handler")
				go s.manager.processResponse(res)
			}
		}
	}
}

type Stack struct {
	udp     *net.UDPAddr
	conn    *net.UDPConn
	manager transactionManager
	running bool
}

func (s *Stack) Local() string {
	return s.conn.LocalAddr().String()
}

func (s *Stack) Bind(a *net.UDPAddr) *Stack {
	s.udp = a
	if s.running {
		s.bindUdp()
	}
	return s
}

func (s *Stack) Register(h InviteHandler) *Stack {
	s.manager.handler = h
	return s
}

func (s *Stack) Start() error {
	log.Debug("Starting stack")
	if s.udp != nil {
		s.bindUdp()
	}
	s.running = true
	log.Info("Stack started")
	return nil
}

func (s *Stack) Stop() {
	s.running = false
	// TODO: create a method to wait for completion
}

func (s *Stack) Invite(sdp, remote, local string, target *net.UDPAddr, handler dialog.Handler) error {
	localUri := uri.NewSip(local, s.Local())
	remoteUri := uri.NewSip(remote, target.String())
	d := dialog.New(localUri, remoteUri, handler, &udpWriter{conn: s.conn, addr: target})
	s.manager.dialogs[d.ID()] = d
	d.Send("INVITE", make(header.List, 0, 0), content.SDP(sdp))
	return nil
}

func NewStack() *Stack {
	return &Stack{
		manager: transactionManager{dialogs: make(map[dialog.Id]*dialog.Dialog)},
		running: false,
	}
}
