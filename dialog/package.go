package dialog

import (
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/content"
	"gitlab.com/jhutchins/go-sip/header"
	"gitlab.com/jhutchins/go-sip/io"
	"gitlab.com/jhutchins/go-sip/message"
	"gitlab.com/jhutchins/go-sip/uri"

)

func New(local, remote *uri.Sip, handler Handler, writer io.Writer) *Dialog {
	return &Dialog{
		id:             Id{callId: uuid.NewV1().String(), localTag: uuid.NewV1().String()},
		localSequence:  1,
		remoteSequence: -1,
		localUri:       local,
		remoteUri:      remote,
		remoteTarget:   remote,
		routes:         nil,
		contact:        local,
		Handler:        handler,
		Writer:         writer,
	}
}

type Id struct {
	callId    string
	localTag  string
	remoteTag string
}

func (i *Id) WithRemoteTag(tag string) Id {
	return Id{
		callId:    i.callId,
		localTag:  i.localTag,
		remoteTag: tag,
	}
}

func IdFromRemote(req *message.Message) Id {
	return Id{
		callId:    req.CallId().Value(),
		localTag:  req.To().Tag(),
		remoteTag: req.From().Tag(),
	}
}

func IdFromLocal(res *message.Message) Id {
	return Id{
		localTag: res.From().Tag(),
		callId:   res.CallId().Value(),
	}
}

func FromRemote(req *message.Message, w io.Writer) *Dialog {
	return &Dialog{
		id:             IdFromRemote(req),
		localSequence:  0,
		remoteSequence: req.CSeq().Sequence(),
		remoteTarget:   req.Contact().URI(),
		routes:         req.GetHeaders("Record-Route"),
		contact:        uri.NewSip(uuid.NewV1().String(), w.Host()),
		Writer:         w,
	}
}

type Handler interface {
	End()
	Established(string)
}

type Dialog struct {
	id             Id
	localSequence  int64
	remoteSequence int64
	localUri       *uri.Sip
	remoteUri      *uri.Sip
	remoteTarget   *uri.Sip
	routes         header.List
	contact        *uri.Sip
	Handler
	io.Writer
}

func (d *Dialog) SetRoutes(routes header.List) *Dialog {
	d.routes = routes
	return d
}

func (d *Dialog) SetId(id Id) *Dialog {
	d.id = id
	return d
}

func (d *Dialog) ID() Id {
	return d.id
}

func (d *Dialog) Contact() *header.Contact {
	return header.NewContact(d.contact.User(), d.contact.Host())
}

func (d *Dialog) Send(method string, headers header.List, payload *content.Content) {
	to := header.NewNameAddr(
		"To",
		header.AbsentString(),
		d.remoteUri,
		map[string]string{"tag": d.id.remoteTag},
	)
	if d.id.remoteTag != "" {
		msg := &message.Message{
			Headers: header.List{
				header.NewVia(d.localUri.Host()),
				header.Numeric("Max-Forwards", 70),
				header.NewNameAddr(
					"From",
					header.AbsentString(),
					d.localUri,
					map[string]string{"tag": d.id.localTag},
				),
				to,
				header.Basic("Call-ID", d.id.callId),
				header.NewCSeq(d.localSequence, method),
				d.Contact(),
			},
		}
		// TODO develop a method to insure safe multi routine access
		d.localSequence++
		msg.Headers = append(msg.Headers, headers...)
		if payload != nil {
			msg.Headers = append(
				msg.Headers,
				payload.TypeHeader(),
				payload.LengthHeader(),
			)
			msg.Body = payload.Content
		} else {
			msg.Headers = append(msg.Headers, header.Numeric("Content-Length", 0))
		}
		d.Writer.Request(&message.Request{
			Method:  method,
			RURI:    d.remoteTarget,
			Message: msg,
		})
	}
}

func (d *Dialog) SendACK(res *message.Response) {
	headers := res.GetHeaders("Via", "v", "From", "f", "To", "t", "Call-ID", "i")
	headers = append(headers,
		d.Contact(),
		res.CSeq().WithMethod("ACK"),
		header.Numeric("Content-Length", 0),
	)
	d.Writer.Request(&message.Request{
		Method:  "ACK",
		RURI:    res.Contact().URI(),
		Message: &message.Message{Headers: headers},
	})
}

type Request struct {
	*message.Request
	*Dialog
}

func (r *Request) Respond(
	status message.Status,
	headers header.List,
	content *content.Content,
) {
	var body string
	log.WithField("content", content).Debug("Creating response")
	if content != nil {
		headers = append(headers,
			header.Basic("Content-Type", content.Kind),
			header.Numeric("Content-Length", len(content.Content)),
		)
		body = content.Content
	} else {
		headers = append(headers, header.Numeric("Content-Length", 0))
		body = ""
	}
	// TODO add the tag from the dialog and use local contact
	headers = append(headers,
		r.CallId(),
		r.To().WithTag(r.id.localTag),
		r.From(),
		r.CSeq(),
		r.Dialog.Contact(),
	)
	for _, header := range r.routes {
		headers = append(headers, header)
	}
	for _, header := range r.GetHeaders("Via", "v") {
		headers = append(headers, header)
	}
	res := &message.Response{
		Status:  &status,
		Message: &message.Message{Headers: headers, Body: body},
	}
	log.WithField("response", res).Debug("Sending response")
	r.Writer.Respond(res)
}
