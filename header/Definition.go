package header

type Definition interface {
	Matches(Instance) bool
}

type compact struct {
	longName    string
	compactName string
}

func (d *compact) Matches(h Instance) bool {
	return h.Name() == d.longName || h.Name() == d.compactName
}

type simple struct {
	name string
}

func (d *simple) Matches(h Instance) bool {
	return h.Name() == d.name
}

func NewDefinition(longName string, compactName ...string) Definition {
	if len(compactName) > 0 {
		return &compact{
			longName:    longName,
			compactName: compactName[0],
		}
	} else {
		return &simple{
			name: longName,
		}
	}
}
