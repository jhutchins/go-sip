package header

import (
	"fmt"

	"gitlab.com/jhutchins/go-sip/uri"
)

func NewNameAddr(
	name string,
	displayName OptionalString,
	uri *uri.Sip,
	params map[string]string,
) *NameAddr {
	return &NameAddr{
		name:        name,
		displayName: displayName,
		uri:         uri,
		params:      params,
	}
}

type NameAddr struct {
	name        string
	displayName OptionalString
	uri         *uri.Sip
	params      map[string]string
}

type OptionalString interface {
	IsPresent() bool
	Value() string
}

type optional struct {
	present bool
	value   string
}

func (o *optional) IsPresent() bool {
	return o.present
}

func (o *optional) Value() string {
	return o.value
}

func AbsentString() OptionalString {
	return &optional{
		present: false,
		value:   "",
	}
}

func String(value string) OptionalString {
	return &optional{
		present: true,
		value:   value,
	}
}

func (h *NameAddr) DisplayName() OptionalString {
	return h.displayName
}

func (h *NameAddr) URI() *uri.Sip {
	return h.uri
}

func (h *NameAddr) Name() string {
	return h.name
}

func (h *NameAddr) Value() string {
	var result string
	if h.DisplayName().IsPresent() {
		result = fmt.Sprintf("\"%s\" ", h.DisplayName().Value())
	}
	result += fmt.Sprintf("<%s>", h.uri)
	for name, val := range h.params {
		result += fmt.Sprintf(";%s=%s", name, val)
	}
	return result
}

func (h *NameAddr) WithName(name string) *NameAddr {
	return &NameAddr{
		name:        name,
		displayName: h.displayName,
		uri:         h.uri,
		params:      h.params,
	}
}

func (h *NameAddr) Tag() string {
	return h.params["tag"]
}

func (h *NameAddr) WithTag(tag string) *NameAddr {
	params := make(map[string]string)
	for key, value := range h.params {
		params[key] = value
	}
	params["tag"] = tag
	return &NameAddr{
		name:        h.name,
		displayName: h.displayName,
		uri:         h.uri,
		params:      params,
	}
}
