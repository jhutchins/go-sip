package header

type basic struct {
	name  string
	value string
}

func Basic(name, value string) Instance {
	return &basic{
		name:  name,
		value: value,
	}
}

func (h *basic) Name() string {
	return h.name
}

func (h *basic) Value() string {
	return h.value
}
