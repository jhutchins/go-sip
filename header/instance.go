package header

type Instance interface {
	Name() string
	Value() string
}

type List []Instance
