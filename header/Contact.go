package header

import (
	"fmt"

	"gitlab.com/jhutchins/go-sip/uri"
)

func NewContact(user, host string) *Contact {
	return &Contact{
		uri: uri.NewSip(user, host),
	}
}

type Contact struct {
	uri *uri.Sip
}

func (h *Contact) Name() string {
	return "Contact"
}

func (h *Contact) Value() string {
	return fmt.Sprintf("%s", h.uri)
}

func (h *Contact) URI() *uri.Sip {
	return h.uri
}
