package header

import "fmt"

type numeric struct {
	name  string
	value int
}

func Numeric(name string, value int) Instance {
	return &numeric{
		name:  name,
		value: value,
	}
}

func (h *numeric) Name() string {
	return h.name
}

func (h *numeric) Value() string {
	return fmt.Sprintf("%d", h.value)
}
