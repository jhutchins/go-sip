package header

import "fmt"

func NewCSeq(sequence int64, method string) *CSeq {
	return &CSeq{
		sequence: sequence,
		method:   method,
	}
}

type CSeq struct {
	sequence int64
	method   string
}

func (h *CSeq) Name() string {
	return "CSeq"
}

func (h *CSeq) Value() string {
	return fmt.Sprintf("%d %s", h.sequence, h.method)
}

func (h *CSeq) Sequence() int64 {
	return h.sequence
}

func (h *CSeq) WithMethod(method string) *CSeq {
	return NewCSeq(h.sequence, method)
}
