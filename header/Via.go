package header

import (
	"fmt"
	"math/rand"
	"time"
)

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func NewVia(host string) Instance {
	return Basic(
		"Via",
		fmt.Sprintf("SIP/2.0/UDP %s;branch=z9hG4bK%v", host, random.Uint32()),
	)
}
