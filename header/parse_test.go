package header

import "testing"

func TestNameAddr(t *testing.T) {
	h := ParseNameAddr("From", "Alice <sip:alice@atlanta.example.com>;tag=9fxced76sl")

	if h == nil {
		t.Fatal("Failed to parse header")
	}

	if !h.DisplayName().IsPresent() {
		t.Error("Expected display name to be present")
	} else if h.DisplayName().Value() != "Alice" {
		t.Errorf("\"%s\" != \"Alice\"", h.DisplayName().Value())
	}

	if h.URI() == nil {
		t.Errorf("Expected URI")
	}

	if h.Tag() != "9fxced76sl" {
		t.Errorf("\"%s\" != \"9fxced76sl\"", h.Tag())
	}
}
