package header

import (
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/parse"
	"gitlab.com/jhutchins/go-sip/uri"
)

func ParseList(ctx *parse.Context) List {
	result := List{}
	for ctx.Left() > 0 && ctx.Peek(2) != "\r\n" {
		log.Debug("Start parsing header")
		ctx.Mark()
		c := ctx.Peek(1)
		for c != " " && c != "\t" && c != ":" {
			ctx.Advance(1)
			c = ctx.Peek(1)
		}
		name := ctx.Segment()
		ctx.Clear()

		parse.Skip(parse.HColon, ctx)

		ctx.Mark()
		c = ctx.Peek(3)
		for len(c) == 3 && (c[:2] != "\r\n" || c[2:] == " " || c[2:] == "\t") {
			ctx.Advance(1)
			c = ctx.Peek(3)
		}
		value := ctx.Segment()

		parse.Skip(parse.CRLF, ctx)

		header := Basic(name, value)
		log.WithField("header", header).Debug("Found header")
		result = append(result, header)

	}
	return result
}

func ParseNameAddr(hName, hValue string) *NameAddr {
	ctx := parse.NewContext(hValue)
	log.WithField("name", hName).Debugf("Start parsing header")

	dName := displayName(ctx)

	// parse URI
	var URI *uri.Sip
	parse.Skip(parse.SWS, ctx)
	if dName.IsPresent() {
		parse.Match("<", ctx)
		URI = parse.Uri(ctx)
		parse.Match(">", ctx)
	} else {
		parse.Skip(eq("<"), ctx)
		URI = parse.Uri(ctx)
		parse.Skip(eq(">"), ctx)
	}
	parse.Skip(parse.SWS, ctx)

	params := map[string]string{}
	for ctx.Peek(1) == ";" {
		log.Debug("Parse a param")
		ctx.Advance(1)
		name, e := parse.Token(ctx)
		if e != nil {
			log.WithError(ctx.Error("failed to find param")).Error("Parse failure")
			return nil
		}
		val := ""
		parse.Skip(parse.SWS, ctx)
		if ctx.Peek(1) == "=" {
			ctx.Advance(1)
			ctx.Mark()
			parse.Skip(parse.SWS, ctx)
			for !strings.Contains(";\r\n", ctx.Peek(1)) {
				ctx.Advance(1)
			}
			val = ctx.Segment()
			ctx.Clear()
		}
		params[name] = val
		log.WithFields(log.Fields{"name": name, "value": val}).Debug("Parse success")
	}
	return NewNameAddr(
		hName,
		dName,
		URI,
		params,
	)
}

func eq(val string) func(*parse.Context) string {
	return func(ctx *parse.Context) string {
		return parse.Match(val, ctx)
	}
}

func displayName(ctx *parse.Context) (result OptionalString) {
	log.Debug("Start parsing display-name")
	defer func() {
		if result.IsPresent() {
			log.WithField("value", result.Value()).Debug("Found")
		} else {
			log.Debug("Not found")
		}
	}()
	ctx.Mark()
	parse.Skip(parse.SWS, ctx)
	if ctx.Peek(1) == "\"" {
		ctx.Advance(1)
		ctx.Mark()
		for ctx.Peek(1) != "\"" && ctx.Left() > 0 {
			ctx.Advance(1)
		}
		if ctx.Peek(1) == "\"" {
			result := ctx.Segment()
			ctx.Clear()
			ctx.Advance(1)
			ctx.Clear()
			return String(result)
		}
	} else {
		for _, e := parse.Token(ctx); e != nil; {
			parse.Skip(parse.LWS, ctx)
		}
		if result := ctx.Segment(); len(result) > 0 {
			ctx.Clear()
			return String(result)
		}
	}
	ctx.Reset()
	return AbsentString()
}
