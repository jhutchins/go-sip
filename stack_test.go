package sip

import (
	"io/ioutil"
	"net"
	"testing"
	"time"

	"gitlab.com/jhutchins/go-sip/dialog"
)

type mockHandler struct{}

func (h *mockHandler) Process(r *dialog.Request) (dialog.Handler, error) {
	return nil, nil
}

func TestStack(t *testing.T) {
	server, _ := net.ResolveUDPAddr("udp", "localhost:7483")
	stack := NewStack().Bind(server).Register(&mockHandler{})
	stack.Start()
	defer stack.Stop()
	conn, _ := net.DialUDP("udp", nil, server)
	data, _ := ioutil.ReadFile("./testing/invite.sip")
	_, _ = conn.Write(data)
	go func() {
		<-time.NewTimer(time.Second).C
		t.Fatal("Test timeout")
	}()
	var buf [2048]byte
	n, _ := conn.Read(buf[0:])
	result := string(buf[0:n])
	expected, _ := ioutil.ReadFile("./testing/trying.sip")
	if string(expected) != result {
		t.Errorf("Expected:\n%s\n\nFound:\n%s", expected, result)
	}
}
