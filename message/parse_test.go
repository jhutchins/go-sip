package message

import (
	"io/ioutil"
	"testing"
)

func TestInvite(t *testing.T) {
	invite, err := ioutil.ReadFile("../testing/invite.sip")
	if err != nil {
		panic(err)
	}
	req, e := ParseRequest(string(invite))
	if e != nil {
		t.Fatalf("Error was %s", e.Error())
	}
	if req.Method != "INVITE" {
		t.Errorf("\"%s\" != \"INVITE\"", req.Method)
	}
	if req.RURI.Secure() {
		t.Errorf("Expected to be insecure")
	}
	if req.RURI.User() != "bob" {
		t.Errorf("\"%s\" != \"bob\"", req.RURI.User())
	}
	if req.RURI.Host() != "biloxi.example.com" {
		t.Errorf("\"%s\" != \"biloxi.example.com\"", req.RURI.Host())
	}
	if len(req.Headers) != 9 {
		t.Errorf("Expected 9 headers got %d", len(req.Headers))
	}
	if len(req.Body) != 151 {
		t.Errorf("Body length was %d expected 151", len(req.Body))
	}
}

func TestRinging(t *testing.T) {
	invite, err := ioutil.ReadFile("../testing/ringing.sip")
	if err != nil {
		panic(err)
	}
	res, e := ParseResponse(string(invite))
	if e != nil {
		t.Fatalf("Error was %s", e.Error())
	}
	if res.Status.Code != "180" {
		t.Errorf("\"%s\" != \"180\"", res.Status.Code)
	}
	if res.Status.Reason != "Ringing" {
		t.Errorf("\"%s\" != \"Ringing\"", res.Status.Reason)
	}
	if len(res.Headers) != 7 {
		t.Errorf("Expected 7 headers got %d", len(res.Headers))
	}
	if len(res.Body) != 0 {
		t.Errorf("Body length was %d expected 0", len(res.Body))
	}
}
