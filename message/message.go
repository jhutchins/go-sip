package message

import (
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/header"
	"gitlab.com/jhutchins/go-sip/uri"
)

type Message struct {
	Headers  header.List
	Body     string
	callerId header.Instance
	from     *header.NameAddr
	to       *header.NameAddr
	cseq     *header.CSeq
	contact  *header.Contact
}

func (m *Message) GetHeaders(names ...string) header.List {
	result := make(header.List, 0, len(names))
	for _, header := range m.Headers {
		for _, name := range names {
			if header.Name() == name {
				result = append(result, header)
				break
			}
		}
	}
	return result
}

func (m *Message) getHeader(def header.Definition) header.Instance {
	for _, header := range m.Headers {
		if def.Matches(header) {
			return header
		}
	}
	return nil
}

func (m *Message) CallId() header.Instance {
	if m.callerId == nil {
		m.callerId = m.getHeader(header.NewDefinition("Call-ID", "i"))
	}
	return m.callerId
}

func (m *Message) From() *header.NameAddr {
	if m.from == nil {
		m.from = m.parseAddr("f", "From")
	}
	return m.from
}

func (m *Message) To() *header.NameAddr {
	if m.to == nil {
		m.to = m.parseAddr("t", "To")
	}
	return m.to
}

func (m *Message) parseAddr(shortName, longName string) *header.NameAddr {
	h := m.getHeader(header.NewDefinition(longName, shortName))
	return header.ParseNameAddr(longName, h.Value())
}

func (m *Message) CSeq() *header.CSeq {
	if m.cseq == nil {
		h := m.getHeader(header.NewDefinition("CSeq"))
		parts := strings.Split(h.Value(), " ")
		if len(parts) == 2 {
			sequence, e := strconv.ParseInt(parts[0], 10, 0)
			if e == nil {
				m.cseq = header.NewCSeq(sequence, strings.Trim(parts[1], " \t\r\n"))
			} else {
				log.Warning("Error converting sequence number: %s", parts[0])
			}
		} else {
			log.Warning("Invalid CSeq header: %s", h.Value)
		}
	}
	return m.cseq
}

func (m *Message) Contact() *header.Contact {
	if m.contact == nil {
		h := m.getHeader(header.NewDefinition("Contact", "m"))
		if h == nil {
			return nil
		}
		n := strings.Index(h.Value(), "sip:")
		protocol := "sips"
		if n == -1 {
			n = strings.Index(h.Value(), "sips:")
		} else {
			protocol = "sip"
		}
		if n != -1 {
			uri := h.Value()[n:]
			if uri[len(uri)-1:] == `"` {
				uri = uri[0 : len(uri)-1]
			}
			n = strings.Index(uri, "@")
			user := ""
			var host string
			if n != -1 {
				user = uri[len(protocol)+1 : n]
				host = uri[n+1:]
			} else {
				host = uri[len(protocol)+1:]
			}
			m.contact = header.NewContact(user, host)
		} else {
			log.Warning("Invalid Contact hader: %s", h.Value)
		}
	}
	return m.contact
}

type Request struct {
	Method string
	RURI   *uri.Sip
	*Message
}

type Status struct {
	Code   string
	Reason string
}

type Response struct {
	Status *Status
	*Message
}
