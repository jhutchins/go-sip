package message

import (
	"strconv"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/header"
	"gitlab.com/jhutchins/go-sip/parse"
)

func ParseRequest(data string) (_ Request, e error) {
	log.Debug("Start parsing sip request")
	defer func() {
		if r := recover(); r != nil {
			e = r.(error)
		}
	}()
	ctx := parse.NewContext(data)

	// Request line
	method, e := parse.Token(ctx)
	if e != nil {
		return
	}
	parse.Skip(parse.SP, ctx)
	uri := parse.Uri(ctx)
	parse.Skip(parse.SP, ctx)
	parse.Skip(version, ctx)
	parse.Skip(parse.CRLF, ctx)

	headers, body := shared(ctx)

	result := Request{Method: method, RURI: uri, Message: &Message{Headers: headers, Body: body}}
	log.WithField("result", result).Debug("Found")
	return result, nil
}

func ParseResponse(data string) (_ *Response, e error) {
	defer func() {
		if r := recover(); r != nil {
			e = r.(error)
		}
	}()
	ctx := parse.NewContext(data)

	// Request line
	parse.Skip(version, ctx)
	parse.Skip(parse.SP, ctx)
	code := statusCode(ctx)
	parse.Skip(parse.SP, ctx)
	reason := reason(ctx)
	parse.Skip(parse.CRLF, ctx)

	headers, body := shared(ctx)

	return &Response{
		Status:  &Status{Code: code, Reason: reason},
		Message: &Message{Headers: headers, Body: body},
	}, nil
}

func shared(ctx *parse.Context) (header.List, string) {
	headers := header.ParseList(ctx)
	data := ""
	if ctx.Left() > 0 {
		parse.Skip(parse.CRLF, ctx)
		data = body(ctx)
	}
	return headers, data
}

func body(ctx *parse.Context) string {
	ctx.Mark()
	ctx.Advance(ctx.Left())
	return ctx.Segment()
}

func version(ctx *parse.Context) string {
	return parse.Match("SIP/2.0", ctx)
}

func reason(ctx *parse.Context) string {
	ctx.Mark()
	defer ctx.Clear()
	for ctx.Peek(2) != "\r\n" {
		ctx.Advance(1)
	}
	return ctx.Segment()
}

func statusCode(ctx *parse.Context) string {
	i, e := strconv.ParseInt(ctx.Peek(3), 10, 64)
	if e != nil || i < 100 {
		panic(ctx.Error("expected status code"))
	}
	return ctx.Advance(3)
}
