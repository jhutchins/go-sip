package parse

import (
	"testing"
	"fmt"
)

func TestString(t *testing.T) {
	ctx := NewContext("testing")
	ctx.Ptr = 3
	if ctx.String() != "ting" {
		t.Errorf("\"%s\" != \"ting\"", ctx.String())
	}
}

func markTest(ctx *Context, t *testing.T) {
	ctx.Ptr = 1
	ctx.Mark()
	if ctx.stack.size != 1 || ctx.stack.top.value != 1 {
		t.FailNow()
	}
	ctx.Ptr = 3
	ctx.Mark()
	if ctx.stack.size != 2 || ctx.stack.top.value != 3 || ctx.stack.top.previous.value != 1 {
		t.FailNow()
	}
}

func TestMark(t *testing.T) {
	ctx := NewContext("testing")
	markTest(ctx, t)
}

func TestReset(t *testing.T) {
	ctx := NewContext("testing")
	markTest(ctx, t)
	ctx.Ptr = 5
	ctx.Reset()
	if  ctx.stack.size != 1 || ctx.stack.top.value != 1 || ctx.Ptr != 3 {
		fmt.Printf("stack: %s", ctx.stack.String())
		t.FailNow()
	}
}

func TestClear(t *testing.T) {
	ctx := NewContext("testing")
	markTest(ctx, t)
	ctx.Ptr = 5
	ctx.Clear()
	if  ctx.stack.size != 1 || ctx.stack.top.value != 1 || ctx.Ptr != 5 {
		fmt.Printf("ctx: %s", ctx)
		t.FailNow()
	}
}

func TestPeek(t *testing.T) {
	ctx := NewContext("testing")
	ctx.Ptr = 2
	if ctx.Peek(2) != "st" || ctx.Ptr != 2 {
		t.FailNow()
	}
}

func TestAdvance(t *testing.T) {
	ctx := NewContext("testing")
	ctx.Ptr = 2
	if ctx.Advance(2) != "st" || ctx.Ptr != 4 {
		t.FailNow()
	}
	if ctx.Advance(10) != "ing" || ctx.Ptr != 7 {
		t.FailNow()
	}
}

func TestInc(t *testing.T) {
	ctx := NewContext("testing")
	ctx.Inc(4)
	if ctx.Ptr != 4 {
		t.FailNow()
	}
	ctx.Inc(10)
	if ctx.Ptr != 7 {
		t.FailNow()
	}
}

func TestLeft(t *testing.T) {
	ctx := NewContext("testing")
	ctx.Ptr = 4
	if ctx.Left() != 3 {
		t.FailNow()
	}
}

func TestSegment(t *testing.T) {
	ctx := NewContext("testing")
	ctx.Mark()
	ctx.Ptr = 4
	if ctx.Segment() != "test" {
		t.FailNow()
	}
}
