package parse

import "fmt"

type stack struct {
	top *node
	size int
}

type node struct {
	previous *node
	value int
}

func (s *stack) push(value int) {
	s.top = &node{s.top, value}
	s.size++
}

func (s *stack) pop() int {
	defer func() {
		s.top = s.top.previous
		s.size--
	}()
	return s.top.value
}

func (s *stack) peek() int {
	return s.top.value
}

func (s *stack) String() string {
	node := s.top
	result := ""
	for node != nil {
		result += fmt.Sprintf("(%d) -> ", node.value)
		node = node.previous
	}
	return result + "nil"
}
