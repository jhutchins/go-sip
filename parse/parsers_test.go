package parse

import "testing"

func TestOptional(t *testing.T) {
	ctx := NewContext("testing")

	r := optional(func(*Context) string {
		panic("fail!")
	}, ctx)
	if r != "" {
		t.Errorf("\"%s\" != \"\"", r)
	}

	r = optional(func(*Context) string {
		return ctx.Advance(4)
	}, ctx)
	if r != "test" {
		t.Errorf("\"%s\" != \"test\"", r)
	}
}

func TestLWS(t *testing.T) {
	r := optional(LWS, NewContext("   "))
	expected := "   "
	if r != expected {
		t.Errorf("\"%s\" != \"%s\"", r, expected)
	}

	r = optional(LWS, NewContext("  \r\n "))
	expected = "  \r\n "
	if r != expected {
		t.Errorf("\"%s\" != \"%s\"", r, expected)
	}

	r = optional(LWS, NewContext("  \r\n\t "))
	expected = "  \r\n\t "
	if r != expected {
		t.Errorf("\"%s\" != \"%s\"", r, expected)
	}

	r = optional(LWS, NewContext("  \r\na"))
	expected = "  "
	if r != expected {
		t.Errorf("\"%s\" != \"%s\"", r, expected)
	}

	r = optional(LWS, NewContext("\r\n\t "))
	expected = "\r\n\t "
	if r != expected {
		t.Errorf("\"%s\" != \"%s\"", r, expected)
	}

	r = optional(LWS, NewContext("\r\n\a"))
	expected = ""
	if r != expected {
		t.Errorf("\"%s\" != \"%s\"", r, expected)
	}
}

func TestToken(t *testing.T) {
	if r, e := Token(NewContext("")); e == nil {
		t.Errorf("Expected error but got \"%s\"", r)
	}

	if r, e := Token(NewContext("token")); e != nil {
		t.Errorf("Got unexpected error \"%+v\"", e)
	} else if r != "token" {
		t.Errorf("\"%s\" != \"token\"", r)
	}

	if r, e := Token(NewContext("tok&en")); e != nil {
		t.Errorf("Got unexpected error \"%+v\"", e)
	} else if r != "tok" {
		t.Errorf("\"%s\" != \"tok\"", r)
	}
}
