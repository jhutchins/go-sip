package parse

import "github.com/pkg/errors"

type Context struct {
	data  string
	Ptr   int
	stack stack
}

func NewContext(data string) *Context {
	return &Context{data: data, Ptr: 0, stack: stack{}}
}

func (c *Context) String() string {
	return c.data[c.Ptr:]
}

func (c *Context) Mark() {
	c.stack.push(c.Ptr)
}

func (c *Context) Reset() {
	c.Ptr = c.stack.pop()
}

func (c *Context) Clear() {
	c.stack.pop()
}

func (c *Context) Peek(length int) string {
	return c.data[c.Ptr : c.Ptr+min(c.Left(), length)]
}

func (c *Context) Advance(length int) string {
	defer c.Inc(length)
	return c.Peek(length)
}

func (c *Context) Inc(length int) {
	c.Ptr += min(c.Left(), length)
}

func (c *Context) Left() int {
	return len(c.data) - c.Ptr
}

func (c *Context) Segment() string {
	return c.data[c.stack.peek():c.Ptr]
}

func (c *Context) Error(msg string) error {
	return errors.Errorf("%s at %d was \"%s\"", msg, c.Ptr, c)
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}
