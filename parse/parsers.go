package parse

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/uri"
)

const tokenChars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.!%*_+`'~"

func Token(ctx *Context) (result string, e error) {
	log.Debug("Start parsing TOKEN")
	defer func() {
		if e != nil {
			log.WithField("result", result).Debug("Found")
		} else {
			log.Debug("Not found")
		}
	}()
	ctx.Mark()
	for ctx.Left() > 0 && strings.Contains(tokenChars, ctx.Peek(1)) {
		ctx.Advance(1)
	}
	result = ctx.Segment()
	if len(result) == 0 {
		defer ctx.Reset()
		return "", ctx.Error("Failed to parse token")
	}
	return result, nil
}

func method(ctx *Context) string {
	ctx.Mark()
	defer ctx.Clear()
	for ctx.Left() > 0 && ctx.Peek(1) != " " {
		ctx.Advance(1)
	}
	result := ctx.Segment()
	if len(result) == 0 {
		panic(ctx.Error("Failed to parse method"))
	}
	return result
}

func SP(ctx *Context) string {
	if ctx.Peek(1) != " " {
		panic(ctx.Error("Not a space"))
	}
	return ctx.Advance(1)
}

func scheme(ctx *Context) string {
	log.Debug("Start parsing uri scheme")
	ctx.Mark()
	for ctx.Peek(1) != ":" && ctx.Left() > 0 {
		ctx.Advance(1)
	}
	scheme := ctx.Segment()
	if scheme != "sip" && scheme != "sips" {
		ctx.Reset()
		panic(ctx.Error("Failed to parse sip uri"))
	}
	ctx.Clear()
	log.WithField("result", scheme).Debug("Found")
	return scheme
}

func user(ctx *Context) string {
	log.Debug("Start parsing uri user")
	ctx.Mark()
	for ctx.Peek(1) != "@" && ctx.Left() > 0 {
		ctx.Advance(1)
	}
	user := ctx.Segment()
	ctx.Clear()
	log.WithField("result", user).Debug("Found")
	return user
}

func Host(ctx *Context) string {
	log.Debug("Start parsing uri host")
	ctx.Mark()
	for ctx.Left() > 0 && !strings.Contains(" >?;", ctx.Peek(1)) {
		ctx.Advance(1)
	}
	host := ctx.Segment()
	ctx.Clear()
	log.WithField("result", host).Debug("Found")
	return host
}

// TODO replace with method in uri.parse.go
func Uri(ctx *Context) *uri.Sip {
	log.Debug("Start parsing uri")
	ctx.Mark()
	scheme := scheme(ctx)
	ctx.Advance(1)
	user := user(ctx)
	ctx.Advance(1)
	host := Host(ctx)
	ctx.Clear()
	result := uri.New(scheme, user, host)
	log.WithField("result", result).Debug("Found")
	return result
}

func CRLF(ctx *Context) string {
	return Match("\r\n", ctx)
}

func Match(str string, ctx *Context) string {
	ctx.Mark()
	ctx.Advance(len(str))
	if ctx.Segment() != str {
		ctx.Reset()
		panic(ctx.Error(fmt.Sprintf("Failed to find \"%s\"", str)))
	}
	return str
}

func wsp(ctx *Context) string {
	c := ctx.Peek(1)
	if c == " " || c == "\t" {
		return ctx.Advance(1)
	}
	panic(ctx.Error("Failed to find WSP"))
}

func LWS(ctx *Context) string {
	ctx.Mark()
	eatWSP(ctx)
	c := ctx.Peek(3)
	if c == "\r\n " || c == "\r\n\t" {
		ctx.Advance(3)
		eatWSP(ctx)
	}
	result := ctx.Segment()
	if len(result) < 1 {
		ctx.Reset()
		panic(ctx.Error("expected LWS"))
	}
	ctx.Clear()
	return result
}

func SWS(ctx *Context) string {
	return optional(LWS, ctx)
}

func HColon(ctx *Context) string {
	ctx.Mark()
	eatWSP(ctx)
	if ctx.Peek(1) != ":" {
		ctx.Reset()
		panic(ctx.Error("expect HCOLON"))
	}
	ctx.Advance(1)
	return ctx.Segment() + SWS(ctx)
}

func eatWSP(ctx *Context) {
	c := ctx.Peek(1)
	for c == " " || c == "\t" {
		ctx.Advance(1)
		c = ctx.Peek(1)
	}
}

func optional(p func(*Context) string, c *Context) string {
	defer func() {
		recover()
	}()
	return p(c)
}

func Skip(p func(*Context) string, c *Context) {
	p(c)
}
