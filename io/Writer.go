package io

import "gitlab.com/jhutchins/go-sip/message"

type Writer interface {
	Host() string
	Respond(*message.Response)
	Request(*message.Request)
}
