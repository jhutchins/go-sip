package uri

import "fmt"

func New(protocol, user, host string) *Sip {
	return &Sip{
		protocol: protocol,
		user:     user,
		host:     host,
	}
}

func NewSip(user, host string) *Sip {
	return New("sip", user, host)
}

type Sip struct {
	protocol string
	user     string
	host     string
}

func (u *Sip) String() string {
	if u.user == "" {
		return fmt.Sprintf("%s:%s", u.protocol, u.host)
	}
	return fmt.Sprintf("%s:%s@%s", u.protocol, u.user, u.host)
}

func (u *Sip) Secure() bool {
	return u.protocol == "sips"
}

func (u *Sip) User() string {
	return u.user
}

func (u *Sip) Host() string {
	return u.host
}
