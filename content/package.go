package content

import "gitlab.com/jhutchins/go-sip/header"

type Content struct {
	Content string
	Kind    string
}

func SDP(content string) *Content {
	return &Content{
		Content: content,
		Kind:    "application/sdp",
	}
}

func (c *Content) TypeHeader() header.Instance {
	return header.Basic("Content-Type", c.Kind)
}

func (c *Content) LengthHeader() header.Instance {
	return header.Numeric("Content-Length", len(c.Content))
}
