package sip

import (
	"runtime/debug"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jhutchins/go-sip/dialog"
	"gitlab.com/jhutchins/go-sip/header"
	"gitlab.com/jhutchins/go-sip/io"
	"gitlab.com/jhutchins/go-sip/message"
)

type InviteHandler interface {
	Process(invite *dialog.Request) (dialog.Handler, error)
}

type transactionManager struct {
	dialogs map[dialog.Id]*dialog.Dialog
	handler InviteHandler
}

func (m *transactionManager) getDialogRequest(req *message.Request, w io.Writer) *dialog.Request {
	localTag := req.To().Tag()
	var d *dialog.Dialog
	if localTag != "" {
		id := dialog.IdFromRemote(req.Message)
		d = m.dialogs[id]
		if d == nil {
			return nil
		}
	} else {
		d = dialog.FromRemote(req.Message, w)
		m.dialogs[d.ID()] = d
	}
	return &dialog.Request{Request: req, Dialog: d}
}

func (m *transactionManager) processRequest(req *message.Request, w io.Writer) {
	defer func() {
		if p := recover(); p != nil {
			var entry *log.Entry
			if err, ok := p.(error); ok {
				entry = log.WithError(err)
			} else {
				entry = log.WithField(log.ErrorKey, p)
			}
			entry.Error("Error processing request")
		}
	}()
	// Kick off a 100 response if not an ACK
	headers := req.GetHeaders("Via", "v", "To", "t", "From", "f", "Call-ID", "i", "CSeq")
	headers = append(headers, header.Numeric("Content-Length", 0))
	if req.Method != "ACK" {
		w.Respond(&message.Response{
			Status:  &message.Status{Code: "100", Reason: "Trying"},
			Message: &message.Message{Headers: headers, Body: ""},
		})
	}

	dialogRequest := m.getDialogRequest(req, w)
	if dialogRequest == nil {
		w.Respond(&message.Response{
			Status:  &message.Status{Code: "481", Reason: "Call/Transaction Does Not Exist"},
			Message: &message.Message{Headers: headers, Body: ""},
		})
		return
	}

	switch req.Method {
	case "ACK":
		m.processAck(dialogRequest)
	case "INVITE":
		m.processInvite(dialogRequest)
	}
}

func (m *transactionManager) processAck(req *dialog.Request) {
	// TODO catch a panic here and send a BYE
	req.Established("")
}

func (m *transactionManager) processInvite(req *dialog.Request) {
	if m.handler != nil {
		handler, e := m.handler.Process(req)
		if e != nil {
			req.Respond(message.Status{"500", "Internal Server Error"}, header.List{}, nil)
			log.Errorf("%+v", e)
		} else {
			req.Handler = handler
		}
	} else {
		req.Respond(message.Status{"405", "Method Not Allowed"}, header.List{header.Basic("Allow", "")}, nil)
	}
}

func (m *transactionManager) processResponse(res *message.Response) {
	defer func() {
		if p := recover(); p != nil {
			log.Errorf("Error processing request: %v\n%s", p, debug.Stack())
		}
	}()
	if res.Status.Code == "200" {
		m.processOk(res)
	}
}

func (m *transactionManager) processOk(res *message.Response) {
	log.Debug("Processing OK response")
	id := dialog.IdFromLocal(res.Message)
	dialog, ok := m.dialogs[id]
	id = id.WithRemoteTag(res.To().Tag())
	if !ok {
		if dialog, ok = m.dialogs[id]; !ok {
			log.Errorf("Cannot find dialog for %s", id)
			return
		}
	} else {
		delete(m.dialogs, dialog.ID())
	}
	dialog.SetRoutes(res.GetHeaders("Record-Route")).SetId(id)
	m.dialogs[id] = dialog
	dialog.SendACK(res)
	dialog.Established(res.Body)
}
